# README #

> "*The [`IOTCODES.ORG`](http://www.iotcodes.org) codes represent a group of artifacts consolidating parts of my work in the past years. Several topics are covered which I consider useful for you, programmers, developers and software engineers.*"

### What is this repository for? ###

he `iotcodes-meta` artifact is a meta artifact used for building the [`IOTCODES.ORG`](http://www.iotcodes.org) artifacts ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) from scratch, usually used by developers. Note that the tools (a bunch of shell scripts) build around this meta artifact are not bundled with this meta artifact.

### How do I get set up? ###

Clone all the repos by invoking `./clone-all.sh` for basic `HTTP` authentication or `./clone-all-ssh.sh` when using `SSH` authentication with public and provate keys.

### Contribution guidelines ###

* Writing tests
* Code review
* Adding functionality
* Fixing bugs

### Who do I talk to? ###

* Siegfried Steiner (steiner@refcodes.org)

### Terms and conditions ###

The [`IOTCODES.ORG`](http://www.iotcodes.org) group of artifacts is published under some open source licenses; covered by the  [`iotcodes-licensing`](https://bitbucket.org/iotcodes/iotcodes-licensing) ([`org.iotcodes`](https://bitbucket.org/iotcodes) group) artifact - evident in each artifact in question as of the `pom.xml` dependency included in such artifact.